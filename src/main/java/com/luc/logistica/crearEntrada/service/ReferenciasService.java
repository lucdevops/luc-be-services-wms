package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Referencias;

public interface ReferenciasService {
	public List<Referencias>findByTexto(String texto);
	public Referencias findByRefe(String codigo);
	 public List<Referencias>  findByRefeProv(String proveedor); 
	/* public Referencias  findByRefeProv(String proveedor); */
	public List<Referencias> findAll();
	public Referencias saveRefe(Referencias refe);
}
