package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Planilla;
import com.luc.logistica.crearEntrada.model.Terceros;
import com.luc.logistica.crearEntrada.repository.PlanillaRepository;
import com.luc.logistica.crearEntrada.service.PlanillaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("planillaService")
public class PlanillaServiceImpl implements PlanillaService {

    @Autowired
    private PlanillaRepository planillaRepository;
  
    @Override
    public Planilla findById(String id) {
        return planillaRepository.findById(id);
    }

    @Override
    public List<Planilla> findAll() {
        return planillaRepository.findAll();
    }

    @Override
    public Planilla save(Planilla planilla) {
        return planillaRepository.save(planilla);
    }

    @Override
    public List<Terceros> findByConductor() {
        return null;
    }

    @Override
    public List<Terceros> findByAyudante() {
        return null;
    }

   
    @Override
    public List<Planilla> findByPD(String f) {
        return planillaRepository.findByPD(f);
    }

    @Override
    public List<Planilla> findByVehiculo(String vehiculo) {
        return planillaRepository.findByVehiculo(vehiculo);
    }

    @Override
    public List<Planilla> findByConductor(String conductor) {
        return planillaRepository.findByConductor(conductor);
    }

    @Override
    public List<Planilla> findByAyudante(String ayudante1) {
        return planillaRepository.findByAyudante(ayudante1);
    }

    @Override
    public List<Planilla> findByAyudante3(String ayudante3) {
        return planillaRepository.findByAyudante3(ayudante3);
    }

    @Override
    public List<Planilla> findByAyudante2(String ayudante2) {
        return planillaRepository.findByAyudante2(ayudante2);
    }

    @Override
    public Planilla findByEstado(String estado, String id) {
        return planillaRepository.findByEstado(estado, id);
    }



  

}
