package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Terceros;
import com.luc.logistica.crearEntrada.repository.TercerosRepository;
import com.luc.logistica.crearEntrada.service.TercerosService;

@Service("terService")
public class TercerosServiceImpl implements TercerosService {

	@Autowired
	private TercerosRepository terRepo;

	@Override
	public Terceros findByTer(String codigoLuc) {
		return terRepo.findByTer(codigoLuc);
	}

	@Override
	public Terceros findByEan(String ean) {
		return terRepo.findByEan(ean);
	}

	@Override
	public Terceros findByNit(Double nit) {
		return terRepo.findByNit(nit);
	}

	@Override
	public List<Terceros> findAll() {
		return terRepo.findAll();
	}

	@Override
	public Terceros saveTer(Terceros ter) {
		terRepo.flush();
		try {
			return terRepo.save(ter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Terceros> findByTexto(String texto) {
		return terRepo.findByTexto(texto);
	}

	@Override
	public List<Terceros> findByDia(String dia) {
		return terRepo.findByDia(dia);
	}

	@Override
	public List<Terceros> findByVidas() {
		return terRepo.findByVidas();
	}

	@Override
	public List<Terceros> findByRuta(Double vendedor) {
		return terRepo.findByRuta(vendedor);
	}

	@Override
	public List<Terceros> findByRutaVendedor(Double vendedor) {
		return terRepo.findByRutaVendedor(vendedor);
	}

	@Override
	public Terceros findByTerTexto(String nombre) {
		return terRepo.findByTerTexto(nombre);
	}

	@Override
	public List<Terceros> findByProveedores(Double nit) {
		return terRepo.findByProvedores(nit);
	}

	@Override
	public List<Terceros> findByAyudante() {
		return terRepo.findByAyudante();
	}

	@Override
	public List<Terceros> findByConductor() {
		return terRepo.findByConductor();
	}

	/* @Override
	public List<Object[]> findCntFacDevProv(Double proveedor, String marca, Integer anio, Integer mes) {
		return terRepo.findCntFacDevProv(proveedor, marca, anio, mes);
	}

	@Override
	public List<Object[]> findCntFacDevCliProv(Double proveedor, String marca, Double nit, Integer anio, Integer mes) {
		return terRepo.findCntFacDevCliProv(proveedor, marca, nit, anio, mes);
	} */

	@Override
	public Terceros saveFechaIFechaF(Terceros fecha) {
		terRepo.flush();
		try {
			return terRepo.save(fecha);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
