package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Ubicaciones;
import com.luc.logistica.crearEntrada.repository.UbicacionesRepository;
import com.luc.logistica.crearEntrada.service.UbicacionesService;

@Service("tubicacionesService")
public class UbicacionesServiceImpl implements UbicacionesService {

    @Autowired
    private UbicacionesRepository ubicacionRepository;

    @Override
    public Ubicaciones findByUbi(String ubicacion) {
        return ubicacionRepository.findByUbi(ubicacion);
    }

    @Override
    public List<Ubicaciones> findAll() {
        return ubicacionRepository.findAll();
    }

    @Override
    public Ubicaciones save(Ubicaciones ubicacion) {
        return ubicacionRepository.save(ubicacion);
    }

}