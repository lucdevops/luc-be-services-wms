package com.luc.logistica.crearEntrada.service;

import com.luc.logistica.crearEntrada.model.PedHis;
import java.util.List;

public interface PedHisService {
	public List<PedHis> findAll();
	public PedHis save(PedHis pedh);
}
