package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Lote;

public interface LoteService {
	public Lote findByLote(String codigo, String lote);
	public List<Lote> findByOrc(String numero);
	public List<Lote> findAll();
	public Lote saveLote(Lote lt);
}

