package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luc.logistica.crearEntrada.model.ClaseVehiculo;
import com.luc.logistica.crearEntrada.repository.ClaseVehiculoRepository;
import com.luc.logistica.crearEntrada.service.ClaseVehiculoService;

@Service("clasevehiculoService")
public class ClaseVehiculoServiceImpl implements ClaseVehiculoService {

    @Autowired
    private ClaseVehiculoRepository clasevehiculoRepository;

    @Override
    public ClaseVehiculo findByNombre(String nombreClase) {
        return clasevehiculoRepository.findByNombre(nombreClase);
    }

    @Override
    public List<ClaseVehiculo> findAll() {
        return clasevehiculoRepository.findAll();
    }

    @Override
    public ClaseVehiculo save(ClaseVehiculo clasevehiculo) {
        // return clasevehiculoRepository.save(clasevehiculo);
        clasevehiculoRepository.flush();
        try {
            return clasevehiculoRepository.save(clasevehiculo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ClaseVehiculo findById(Integer idClase) {
        return clasevehiculoRepository.findById(idClase);
    }
}
