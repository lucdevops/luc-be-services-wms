package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Referencias;
import com.luc.logistica.crearEntrada.repository.ReferenciasRepository;
import com.luc.logistica.crearEntrada.service.ReferenciasService;

@Service("refeService")
public class ReferenciasServiceImpl implements ReferenciasService{
	@Autowired
	private ReferenciasRepository refeRepo;
	
	@Override
	public Referencias findByRefe(String codigo) {
		return refeRepo.findByRefe(codigo);
	}
	
	@Override
	public List<Referencias> findAll() {
		return refeRepo.findAll();
	}
	
	@Override
	public Referencias saveRefe(Referencias refe) {
		refeRepo.flush();
		try {
			return refeRepo.save(refe);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public List<Referencias> findByTexto(String texto) {
		return refeRepo.findByTexto(texto);
	}

 	@Override
	public List<Referencias>  findByRefeProv(String proveedor) {
		return refeRepo.findByRefeProv(proveedor);
	} 

/* 	@Override
	public Referencias  findByRefeProv(String proveedor) {
		return refeRepo.findByRefeProv(proveedor);
	} */
}
