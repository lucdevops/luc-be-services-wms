package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.ReferenciasTipo;

public interface ReferenciasTipoService {

	public ReferenciasTipo findByTipo(String tipo);
	public List<ReferenciasTipo> findAll();
	public ReferenciasTipo save(ReferenciasTipo refeTipo);
}
