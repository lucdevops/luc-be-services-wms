package com.luc.logistica.crearEntrada.service;

import com.luc.logistica.crearEntrada.model.Ped;

import java.util.Date;
import java.util.List;

public interface PedService {
	public Ped findbyPed(Integer numero, String tipo);

	public Ped findbyId(String id);

	public List<Ped> findExistente(String proveedor, Date back, Date today);

	public List<Ped> findEnProceso();

	public List<Ped> findEnTransito();

	public List<Ped> findCliAgen(String proveedor);

	public List<Ped> findAll();

	public Ped savePed(Ped ped);
}
