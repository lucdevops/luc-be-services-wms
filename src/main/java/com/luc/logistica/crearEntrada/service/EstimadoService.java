package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Estimado;

public interface EstimadoService {
    public Estimado findByEst(Integer ano, Integer mes, String tipo, Double cliente, String referencia);
    //public List<Object> findByPeriodoMarca(Integer ano,Integer mes);
    public List<Object[]> findPrueba(String marca, Integer mes);
    public List<Object[]> findEstRef(Integer mes, String codigo);
    public List<Object[]> findPresupuestoProveedor(Double vendedor, Integer mes);
    public List<Object[]> findPresupuestoCliente(Double vendedor, Integer mes);
    public List<Object[]> findDetalleCliente(Double vendedor, Integer mes, Double nit);
    public Double findStockActual(String codigo);
    public List<Estimado> findAll();
	public Estimado save(Estimado est);
}

