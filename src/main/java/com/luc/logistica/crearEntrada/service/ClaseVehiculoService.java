package com.luc.logistica.crearEntrada.service;

import java.util.List;

import com.luc.logistica.crearEntrada.model.ClaseVehiculo;

public interface ClaseVehiculoService {
	
	public ClaseVehiculo findByNombre(String nombreClaseV);
	public ClaseVehiculo findById(Integer idClase);
	public List<ClaseVehiculo> findAll();
	public ClaseVehiculo save(ClaseVehiculo clasevehiculo);
}
