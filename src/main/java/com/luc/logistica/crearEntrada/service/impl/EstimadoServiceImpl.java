package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Estimado;
import com.luc.logistica.crearEntrada.repository.EstimadoRepository;
import com.luc.logistica.crearEntrada.service.EstimadoService;

@Service("estimadoService")
public class EstimadoServiceImpl implements EstimadoService {

	@Autowired
	private EstimadoRepository estRepository;

	@Override
	public List<Estimado> findAll() {
		return estRepository.findAll();
	}

	@Override
	public Estimado save(Estimado est) {
		return estRepository.save(est);
	}

	@Override
	public Estimado findByEst(Integer ano, Integer mes, String tipo, Double cliente, String referencia) {
		return estRepository.findByEst(ano, mes, tipo, cliente, referencia);
	}

	/*@Override
	public List<Object> findByPeriodoMarca(Integer ano, Integer mes) {
		return estRepository.findByPeriodoMarca(ano, mes);
	}*/

	@Override
	public List<Object[]> findPrueba(String marca, Integer mes) {
		return estRepository.findPrueba(marca, mes);
	}

	@Override
	public Double findStockActual(String codigo) {
		return estRepository.findStockActual(codigo);
	}

	@Override
	public List<Object[]> findEstRef(Integer mes, String codigo) {
		return estRepository.findEstRef(mes, codigo);
	}

	@Override
	public List<Object[]> findPresupuestoProveedor(Double vendedor, Integer mes) {
		return estRepository.findPresupuestoProveedor(vendedor, mes);
	}

	
	@Override
	public List<Object[]> findPresupuestoCliente(Double vendedor, Integer mes) {
		return estRepository.findPresupuestoCliente(vendedor, mes);
	}

	@Override
	public List<Object[]> findDetalleCliente(Double vendedor, Integer mes, Double nit) {
		return estRepository.findDetalleCliente(vendedor, mes, nit);
	}

	

}
