package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.DetalleOCPedHis;

public interface DetalleOCPedHisService {
	public DetalleOCPedHis findById(Integer numero);
	public List<DetalleOCPedHis> findAll();
	public DetalleOCPedHis saveDetOCPedHis(DetalleOCPedHis docPedHis);
}