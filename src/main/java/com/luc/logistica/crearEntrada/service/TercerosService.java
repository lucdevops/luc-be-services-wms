package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Terceros;

public interface TercerosService {
	public Terceros findByTer(String codigoLuc);

	public Terceros findByEan(String ean);

	public Terceros findByNit(Double nit);

	public Terceros findByTerTexto(String nombre);

	public List<Terceros> findByRuta(Double vendedor);

	public List<Terceros> findByRutaVendedor(Double vendedor);

	public List<Terceros> findByVidas();

	public List<Terceros> findAll();

	public List<Terceros> findByTexto(String texto);

	public List<Terceros> findByDia(String dia);

/* 	public List<Object[]> findCntFacDevProv(Double proveedor, String marca, Integer anio, Integer mes);

	public List<Object[]> findCntFacDevCliProv(Double proveedor, String marca, Double nit, Integer anio, Integer mes); */

	public Terceros saveTer(Terceros ter);

	public Terceros saveFechaIFechaF(Terceros id);

	public List<Terceros> findByProveedores(Double nit);

	//public List<Terceros> findByAlmacen();

	public List<Terceros> findByAyudante();

	public List<Terceros> findByConductor();
}
