package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luc.logistica.crearEntrada.model.Vehiculo;
import com.luc.logistica.crearEntrada.repository.VehiculoRepository;
import com.luc.logistica.crearEntrada.service.VehiculoService;

@Service("vehiculoService")
public class VehiculoServiceImpl implements VehiculoService {

    @Autowired
    private VehiculoRepository vehiculoRepository;

    @Override
    public Vehiculo findById(String placa) {
        return vehiculoRepository.findById(placa);

    }

    @Override
    public List<Vehiculo> findAll() {
        return vehiculoRepository.findAll();
    }

    /*
     * @Override public Vehiculo save(Vehiculo vehiculo) { return
     * vehiculoRepository.save(vehiculo); }
     */

    @Override
    public Vehiculo save(Vehiculo vehiculo) {
        vehiculoRepository.flush();
        try {
            return vehiculoRepository.save(vehiculo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * @Override public List<Vehiculo> findByVehiculo(String placa) { // TODO
     * Auto-generated method stub return vehiculoRepository.findByVehiculo(placa); }
     */

    /*
     * @Override public List<Vehiculo> findByVehiculo() { return
     * vehiculoRepository.findByVehiculo(); }
     */

}
