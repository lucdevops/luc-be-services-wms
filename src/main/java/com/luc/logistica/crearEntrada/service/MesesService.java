package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Meses;

public interface MesesService {
	public Meses findByMes(String mes);
	public List<Meses> findAll();
	public Meses save(Meses marca);
}
