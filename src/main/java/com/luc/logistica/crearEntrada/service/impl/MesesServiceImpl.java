package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Meses;
import com.luc.logistica.crearEntrada.repository.MesesRepository;
import com.luc.logistica.crearEntrada.service.MesesService;

@Service("mesesService")
public class MesesServiceImpl implements MesesService {

    @Autowired
    private MesesRepository mesesRepository;

    @Override
    public Meses findByMes(String mes) {
        return mesesRepository.findByMes(mes);
    }

    @Override
    public List<Meses> findAll() {
        return mesesRepository.findAll();
    }

    @Override
    public Meses save(Meses meses) {
        return mesesRepository.save(meses);
    }

}