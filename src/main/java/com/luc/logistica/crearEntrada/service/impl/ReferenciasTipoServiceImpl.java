package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.ReferenciasTipo;
import com.luc.logistica.crearEntrada.repository.ReferenciasTipoRepository;
import com.luc.logistica.crearEntrada.service.ReferenciasTipoService;

@Service("refeTipoService")
public class ReferenciasTipoServiceImpl implements ReferenciasTipoService {
    @Autowired
    private ReferenciasTipoRepository refeTipoRepo;

    @Override
    public List<ReferenciasTipo> findAll() {
        return refeTipoRepo.findAll();
    }

    @Override
    public ReferenciasTipo save(ReferenciasTipo refe) {
        return refeTipoRepo.save(refe);
    }

    @Override
    public ReferenciasTipo findByTipo(String tipo) {
        return refeTipoRepo.findByTipo(tipo);
    }

}
