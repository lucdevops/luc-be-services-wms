package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Um;;

public interface UmService {
	public List<Um> findById(String codigo);
	public List<Um> findAll();
	public Um findByUndMin(String codigo, String unidad);
	public Um save(Um um);
}
