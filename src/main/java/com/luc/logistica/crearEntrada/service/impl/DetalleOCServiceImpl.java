package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.DetalleOC;
import com.luc.logistica.crearEntrada.repository.DetalleOCRepository;
import com.luc.logistica.crearEntrada.service.DetalleOCService;

@Service("detOrdenCompraService")
public class DetalleOCServiceImpl implements DetalleOCService {

	@Autowired
	private DetalleOCRepository docRepo;

	@Override
	public DetalleOC findById(Integer numero) {
		return docRepo.findById(numero);
	}

	@Override
	public List<DetalleOC> findAll() {
		return null;
	}

	@Override
	public void saveDetOC(DetalleOC doc) {
		docRepo.flush();
		try {
			docRepo.saveAndFlush(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object findByPromVenta(String tipo, String codigo, Integer anio, Integer mes) {
		return docRepo.findByPromVenta(tipo, codigo, anio, mes);
	}

	@Override
	public Object findByCount(String tipo, String codigo, Integer anio, Integer mes) {
		return docRepo.findByCount(tipo, codigo, anio, mes);
	}

	/*@Override
	public List<DetalleOC> findByPromVenta(String tipo, String codigo) {
		return docRepo.findByPromVenta(tipo, codigo);
	}*/

}
