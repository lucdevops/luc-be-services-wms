package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.User;
import com.luc.logistica.crearEntrada.repository.UserRepository;
import com.luc.logistica.crearEntrada.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void saveUser(User user) {
		user.setPassword(user.getPassword());
		userRepository.save(user);
	}

	@Override
	public User findUserByUserName(String username) {
		return userRepository.findByUserName(username);
	}


	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findByUserPass(String username, String password) {
		return userRepository.findByUserPass(username, password);
	}

}