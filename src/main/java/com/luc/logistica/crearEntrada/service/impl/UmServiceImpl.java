package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Um;
import com.luc.logistica.crearEntrada.repository.UmRepository;
import com.luc.logistica.crearEntrada.service.UmService;

@Service("umService")
public class UmServiceImpl implements UmService{
	
	@Autowired
	private UmRepository umRepository;
	
	@Override
	public List<Um> findById(String codigo) {
		return umRepository.findById(codigo);
	}

	@Override
	public List<Um> findAll() {
		return umRepository.findAll();
	}


	@Override
	public Um save(Um um) {
		return umRepository.save(um);
	}

	@Override
	public Um findByUndMin(String codigo, String unidad) {
		return umRepository.findByUndMin(codigo, unidad);
	}

	
}