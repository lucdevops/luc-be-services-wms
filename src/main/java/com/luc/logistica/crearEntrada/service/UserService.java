package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.User;

public interface UserService {
	public User findUserByUserName(String username);
	public void saveUser(User user);
	public List<User> findAll();
	public User findByUserPass(String username, String password);
}