package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Estimado;
import com.luc.logistica.crearEntrada.model.EstimadoKey;

@Repository("estimadoRepository")
public interface EstimadoRepository extends JpaRepository<Estimado, EstimadoKey> {

    @Query("select est from Estimado est where est.llave.ano = ?1 AND est.llave.mes = ?2 AND est.llave.tipo = ?3 AND est.llave.cliente.nit = ?4 AND est.llave.referencia.codigo = ?5")
    Estimado findByEst(Integer ano, Integer mes, String tipo, Double cliente, String referencia);

    /*@Query("select e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.marca, e.llave.referencia.marca.nombre, ROUND(sum(e.estimado) / 1000,0) as pron from Estimado e  where e.llave.ano = ?1 and e.llave.mes = ?2 GROUP BY e.llave.tipo,e.llave.ano,e.llave.mes,e.llave.referencia.marca.id,e.llave.referencia.marca.nombre")
    List<Object> findByPeriodoMarca(Integer ano, Integer mes);*/

    @Query("select e.llave.tipo as tipo, e.llave.ano as anio, e.llave.mes as mes, e.llave.referencia.codigo as codigo, r.descripcion as descripcion, r.tipo as rTipo, rt.dias as dias, sum(e.cantidad) as cantidad from Estimado e, Referencias r, ReferenciasTipo rt where r.marca = ?1 and e.llave.mes = ?2 and e.llave.referencia.codigo = r.codigo and r.tipo = rt.tipo group by e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.codigo, r.descripcion, r.tipo, rt.dias ORDER BY r.tipo")
    List<Object[]> findPrueba(String marca, Integer mes);

    @Query("select e.llave.referencia.codigo as codigo, r.descripcion as descripcion, r.tipo as rTipo, rt.dias as dias, sum(e.cantidad) as cantidad from Estimado e, Referencias r, ReferenciasTipo rt where e.llave.mes = ?1 and r.codigo = ?2 and e.llave.referencia.codigo = r.codigo and r.tipo = rt.tipo group by e.llave.referencia.codigo, r.descripcion, r.tipo, rt.dias ORDER BY r.tipo")
    List<Object[]> findEstRef(Integer mes, String codigo);

    @Query("select sum(pu.cntEntrada - pu.cntSalida) as saldo from Ubicaciones u, ProdUbicado pu, Referencias r where r.codigo = ?1 and u.id = pu.idUbi and r.id = pu.idRefe and (pu.cntEntrada - pu.cntSalida) != 0")
    Double findStockActual(String codigo);

    @Query("select m.id, m.nombre, SUM(e.cantidad) from Estimado e, Marca m, Terceros t, Referencias r where t.vendedor = ?1 and e.llave.mes = ?2 and e.llave.cliente.nit = t.nit and e.llave.referencia.codigo = r.codigo and r.marca = m.id GROUP BY m.id, m.nombre HAVING COUNT(*)>=1")
    List<Object[]> findPresupuestoProveedor(Double vendedor, Integer mes);

    @Query("select t.nit, t.nombres, SUM(e.cantidad) from Estimado e, Terceros t where t.vendedor = ?1 and e.llave.mes = ?2 and e.llave.cliente.nit = t.nit  GROUP BY t.nit, t.nombres HAVING COUNT(*)>=1")
    List<Object[]> findPresupuestoCliente(Double vendedor, Integer mes);

    @Query("select m.nombre, SUM(e.cantidad) from Estimado e, Marca m, Terceros t, Referencias r where t.vendedor = ?1 and e.llave.mes = ?2 and t.nit = ?3 and e.llave.cliente.nit = t.nit and e.llave.referencia.codigo = r.codigo and r.marca = m.id GROUP BY m.nombre HAVING COUNT(*)>=1")
    List<Object[]> findDetalleCliente(Double vendedor, Integer mes, Double nit);
}