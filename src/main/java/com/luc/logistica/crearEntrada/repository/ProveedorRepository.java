package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Proveedor;

@Repository("proveedorRepository")
public interface ProveedorRepository extends JpaRepository<Proveedor, String> {
	
	 @Query("select p from Proveedor p where p.id = ?1")
	 Proveedor findById(String id);
	 
	 @Query("select p from Proveedor p where p.nombre = ?1")
	 Proveedor findByName(String nombre);

	 @Query("select p from Proveedor p where p.bodega = ?1")
	 Proveedor findByBodega(String bodega);
}