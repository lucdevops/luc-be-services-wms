package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.ReferenciasTipo;

@Repository("refeTipoRepository")
public interface ReferenciasTipoRepository extends JpaRepository<ReferenciasTipo, String> {

    @Query("select rt from ReferenciasTipo rt where rt.tipo = ?1")
    ReferenciasTipo findByTipo(String tipo);

}
