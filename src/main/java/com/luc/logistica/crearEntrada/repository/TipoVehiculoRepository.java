package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.TipoVehiculo;

@Repository("tipovehiculoRepository")
public interface TipoVehiculoRepository extends JpaRepository<TipoVehiculo, Integer> {
	
	 @Query("select tv from TipoVehiculo tv where tv.nombreVehiculo = ?1")
	 TipoVehiculo findByNombre(String nombreVehiculo);
	 
	 @Query("select tv from TipoVehiculo tv where tv.idVehiculo = ?1")
	 TipoVehiculo findById(Integer idVehiculo);
	
}
