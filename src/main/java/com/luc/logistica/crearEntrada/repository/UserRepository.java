package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {
	 @Query("select u from User u where u.id = ?1")
	 User findByUserName(String username);


	 @Query("select u from User u where u.id = ?1 and u.password = ?2")
	 User findByUserPass(String username, String password);
	
}