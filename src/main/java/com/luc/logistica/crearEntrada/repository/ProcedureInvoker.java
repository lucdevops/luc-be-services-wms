package com.luc.logistica.crearEntrada.repository;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("procedureRepository")
public class ProcedureInvoker {

	private final EntityManager entityManager;

	@Autowired
	public ProcedureInvoker(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public long distribuirCuota(int linea, double estimado, int ano, int mes, String tipo, Double nit,
			String referencia) {

		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH) + 1;

		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("dbo.SP_LUC_DISTRIBUIR_CUOTA");
		/*
		 * @IPDLIN DECIMAL,
		 * 
		 * @IPDESTIMADO DECIMAL,
		 * 
		 * @IPNANO NUMERIC,
		 * 
		 * @IPNMES NUMERIC,
		 * 
		 * @IPVTIPO VARCHAR(50),
		 * 
		 * @IPDCLIENTE DECIMAL,
		 * 
		 * @IPVREFCOD VARCHAR(20)
		 */
		storedProcedureQuery.registerStoredProcedureParameter(1, Double.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(2, Double.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(6, Double.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(7, String.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(8, Integer.class, ParameterMode.IN);

		storedProcedureQuery.setParameter(1, new Double(linea));
		storedProcedureQuery.setParameter(2, new Double(estimado));
		storedProcedureQuery.setParameter(3, new Integer(ano));
		storedProcedureQuery.setParameter(4, new Integer(mes));
		storedProcedureQuery.setParameter(5, tipo);
		storedProcedureQuery.setParameter(6, nit);
		storedProcedureQuery.setParameter(7, referencia);
		storedProcedureQuery.setParameter(8, new Integer(month));
		storedProcedureQuery.execute();
		// final Long outputValue2 = (Long)
		// storedProcedureQuery.getOutputParameterValue(8);
		return 1;

	}

	public long iniciaEstimado(int ano, int mes) {

		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("dbo.SP_LUC_MARCAS_ESTIMADO");
		/*
		 * @IPNANO NUMERIC,
		 * 
		 * @IPNMES NUMERIC
		 */
		storedProcedureQuery.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);

		storedProcedureQuery.setParameter(1, new Integer(ano));
		storedProcedureQuery.setParameter(2, new Integer(mes));
		storedProcedureQuery.execute();
		// final Long outputValue2 = (Long)
		// storedProcedureQuery.getOutputParameterValue(8);
		return 1;

	}
}