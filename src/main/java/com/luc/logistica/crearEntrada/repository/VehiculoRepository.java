package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Vehiculo;

@Repository("vehiculoRepository")
public interface VehiculoRepository extends JpaRepository<Vehiculo, String> {
	
	 @Query("select v from Vehiculo v where v.placa = ?1")
	 Vehiculo findById(String placa);
	
	
}