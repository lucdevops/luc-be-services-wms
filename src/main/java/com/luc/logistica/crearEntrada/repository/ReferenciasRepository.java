package com.luc.logistica.crearEntrada.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Referencias;

@Repository("refeRepository")
public interface ReferenciasRepository extends JpaRepository<Referencias, String> {

	@Query("select re from Referencias re where re.codigo = ?1")
	Referencias findByRefe(String codigo);

 	@Query("select re from Referencias re where re.marca.id = ?1")
	List<Referencias> findByRefeProv(String proveedor); 

/* 	@Query("select re from Referencias re where re.marca.id = ?1")
	Referencias findByRefeProv(String proveedor); */

	@Query("SELECT re FROM Referencias re WHERE re.descripcion like %?1%")
	List<Referencias> findByTexto(String texto);

	/*
	 * @Query("SELECT re FROM Referencias re WHERE (re.codigo like %?1%)")
	 * List<Referencias> findByTexto(String texto);
	 */
}
