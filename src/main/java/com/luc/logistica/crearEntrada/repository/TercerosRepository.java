package com.luc.logistica.crearEntrada.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Terceros;

@Repository("terRepository")
public interface TercerosRepository extends JpaRepository<Terceros, Double> {

	@Query("select t from Terceros t where t.codigoLuc = ?1")
	Terceros findByTer(String codigoLuc);

	@Query("select t from Terceros t where t.ean = ?1")
	Terceros findByEan(String ean);

	@Query("select t from Terceros t where t.nit = ?1")
	Terceros findByNit(Double nit);

	@Query("select t from Terceros t where t.nombres = ?1")
	Terceros findByTerTexto(String nombre);

	@Query("SELECT t FROM Terceros t WHERE (t.nombres like %?1% OR t.codigoLuc like %?1%)")
	List<Terceros> findByTexto(String texto);

	@Query("SELECT t FROM Terceros t WHERE t.diaRecibo = ?1 ORDER BY t.nombres ASC")
	List<Terceros> findByDia(String dia);

	@Query("select t from Terceros t where t.concep1 = '18'")
	List<Terceros> findByVidas();

	@Query("select t from Terceros t where t.vendedor = ?1")
	List<Terceros> findByRuta(Double vendedor);

	@Query("select t from Terceros t where t.vendedor = ?1 and t.codigoLuc != t.ean ORDER BY t.nombres ASC")
	List<Terceros> findByRutaVendedor(Double vendedor);

	
	/* @Query("select r.marca.id, d.docKey.tipo, SUM(dl.cantidad) from EncabezadoOC d, DetalleOC dl, Referencias r, Terceros t where ((d.docKey.tipo = 'FAC' and d.estado = 'FA') or (d.docKey.tipo like '%DV%')) and d.docKey.numero = dl.numero and d.tercero.nit = t.nit and t.vendedor = ?1 and dl.refe.codigo = r.codigo and r.marca.id = ?2 and YEAR(d.fecha) = ?3 and MONTH(d.fecha) = ?4 group by r.marca.id, d.docKey.tipo")
	List<Object[]> findCntFacDevProv(Double proveedor, String marca, Integer anio, Integer mes);

	@Query("select r.marca.id, d.docKey.tipo, SUM(dl.cantidad) from EncabezadoOC d, DetalleOC dl, Referencias r, Terceros t where ((d.docKey.tipo = 'FAC' and d.estado = 'FA') or (d.docKey.tipo like '%DV%')) and d.docKey.numero = dl.numero and d.tercero.nit = t.nit and t.vendedor = ?1 and dl.refe.codigo = r.codigo and r.marca.id = ?2 and d.tercero.nit = ?3 and YEAR(d.fecha) = ?4 and MONTH(d.fecha) = ?5 group by r.marca.id, d.docKey.tipo")
	List<Object[]> findCntFacDevCliProv(Double proveedor, String marca, Double nit, Integer anio, Integer mes);
 */
	/*@Query("select r.nombre, d.docKey.tipo, SUM(dl.cantidad) as cantidad from EncabezadoOC d, DetalleOC dl, Referencias r, Terceros t where ((d.docKey.tipo like 'FAC' and d.estado = 'FA') or (d.docKey.tipo like '%dv%')) and d.docKey.numero = dl.numero and d.nit = t.nit and t.vendedor = ?1 and dl.refe.codigo = r.codigo  and d.terceros.nit = ?2 and d.fecha BETWEEN ?3 AND ?4 group by r.marca, d.docKey.tipo")
	List<Object[]> findCntFacDevCli(Integer numero, Integer nit, String fechaI, String fechaF); */

	@Query("select t from Terceros t WHERE t.concep1 = '16' OR t.concep1 = '17'")
	List<Terceros> findByAyudante();
	
	@Query("select t from Terceros t WHERE t.concep1 = '20' OR t.concep1 = '17'")
	List<Terceros> findByConductor();

	/*@Query("select t from Terceros t where t.nombres = ?1")
	List<Terceros> findByAlmacen();*/

	@Query("select t from Terceros t where t.nitReal = ?1")
	List<Terceros> findByNitCli(Double nitReal);

	@Query("select t.provRey, m.nombre from Terceros t, Marca m where t.nit = ?1 and t.provRey = m.id")
	List<Terceros> findByProvedores(Double nit);
	

}
