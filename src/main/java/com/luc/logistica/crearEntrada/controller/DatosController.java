package com.luc.logistica.crearEntrada.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.luc.logistica.crearEntrada.model.ClaseVehiculo;
import com.luc.logistica.crearEntrada.model.DetalleOC;
import com.luc.logistica.crearEntrada.model.DetalleOCPed;
import com.luc.logistica.crearEntrada.model.Marca;
import com.luc.logistica.crearEntrada.model.Meses;
import com.luc.logistica.crearEntrada.model.Ped;
import com.luc.logistica.crearEntrada.model.PedHis;
import com.luc.logistica.crearEntrada.model.PedKey;
import com.luc.logistica.crearEntrada.model.Planilla;
import com.luc.logistica.crearEntrada.model.PlanillaLin;
import com.luc.logistica.crearEntrada.model.PreOrden;
import com.luc.logistica.crearEntrada.model.Referencias;
import com.luc.logistica.crearEntrada.model.ReferenciasTipo;
import com.luc.logistica.crearEntrada.model.Terceros;
import com.luc.logistica.crearEntrada.model.TipoVehiculo;
import com.luc.logistica.crearEntrada.model.Um;
import com.luc.logistica.crearEntrada.model.User;
import com.luc.logistica.crearEntrada.model.Vehiculo;
import com.luc.logistica.crearEntrada.repository.ProcedureInvoker;
import com.luc.logistica.crearEntrada.service.ClaseVehiculoService;
import com.luc.logistica.crearEntrada.service.DetalleOCPedService;
import com.luc.logistica.crearEntrada.service.DetalleOCService;
import com.luc.logistica.crearEntrada.service.EstimadoService;
import com.luc.logistica.crearEntrada.service.MarcaService;
import com.luc.logistica.crearEntrada.service.MesesService;
import com.luc.logistica.crearEntrada.service.PedHisService;
import com.luc.logistica.crearEntrada.service.PedService;
import com.luc.logistica.crearEntrada.service.PlanillaLinService;
import com.luc.logistica.crearEntrada.service.PlanillaService;
import com.luc.logistica.crearEntrada.service.ReferenciasService;
import com.luc.logistica.crearEntrada.service.ReferenciasTipoService;
import com.luc.logistica.crearEntrada.service.TercerosService;
import com.luc.logistica.crearEntrada.service.TipoVehiculoService;
import com.luc.logistica.crearEntrada.service.UmService;
import com.luc.logistica.crearEntrada.service.UserService;
import com.luc.logistica.crearEntrada.service.VehiculoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

@RestController
@CrossOrigin(origins = { "*", "*" })
// @CrossOrigin(origins = { "http://192.168.1.250:8083", "*" })
@RequestMapping("/datos")

public class DatosController {

	@Autowired
	private TercerosService tercerosService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private ReferenciasService refeService;
	@Autowired
	private ReferenciasTipoService refeTipoService;
	@Autowired
	private PedService pedService;
	@Autowired
	private DetalleOCPedService detPedService;
	@Autowired
	private ProcedureInvoker procedureRepository;
	@Autowired
	private EstimadoService estimadoService;
	@Autowired
	private MesesService mesService;
	@Autowired
	private PedHisService pedHisService;
	@Autowired
	private DetalleOCPedService docPedService;
	@Autowired
	private UmService umService;
	@Autowired
	private UserService userService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private VehiculoService vehiculoService;
	@Autowired
	private ClaseVehiculoService clasevehiculoService;
	@Autowired
	private TipoVehiculoService tipovehiculoService;
	@Autowired
	private PlanillaService planillaService;
	@Autowired
	private PlanillaLinService planillaLinService;
	@Autowired
	private DetalleOCService detOcServices;

	@GetMapping("/cargaEstimado/{ano}/{mes}/{linea}/{estimadomes}/{cliente}/{referencia}")
	public long cargaEstimado(@PathVariable("ano") String ano, @PathVariable("mes") String mes,
			@PathVariable("linea") Integer linea, @PathVariable("estimadomes") Integer estimadomes,
			@PathVariable Object cliente, @PathVariable Object referencia) {

		String marca = "";
		String tipo = "";
		List<Referencias> ref = new ArrayList<Referencias>();
		Marca m = null;

		if (linea != 0) {
			if (linea < 10) {
				marca = "0" + linea;
			}

			if (marca.equals("01") || marca.equals("02") || marca.equals("09") || marca.equals("10")) {
				tipo = "LUC";
			} else {
				tipo = "REP";
			}

		} else {
			// ref = refeService.findByRefe(String.valueOf(referencia));
			ref = refeService.findByTexto(String.valueOf(referencia));
			m = marcaService.findById(ref.get(0).getMarca().getId());

			if (m.getId().equals("01") || m.getId().equals("02") || m.getId().equals("09") || m.getId().equals("10")) {
				tipo = "LUC";
			} else {
				tipo = "REP";
			}
		}

		// Marca m = marcaService.findById(marca);
		// String cli = String.valueOf(cliente);
		List<Terceros> ter = tercerosService.findByTexto(String.valueOf(cliente));

		if (ter.size() == 0 && ref.size() == 0) {
			return procedureRepository.distribuirCuota(linea, estimadomes, Integer.parseInt(ano), Integer.parseInt(mes),
					tipo, null, null);
		} else if (ter.size() > 0 && ref.size() == 0) {
			return procedureRepository.distribuirCuota(linea, estimadomes, Integer.parseInt(ano), Integer.parseInt(mes),
					tipo, ter.get(0).getNit(), null);
		} else if (ter.size() == 0 && !ref.get(0).getCodigo().equals("null")) {
			return procedureRepository.distribuirCuota(Integer.parseInt(m.getId()), estimadomes, Integer.parseInt(ano),
					Integer.parseInt(mes), tipo, null, ref.get(0).getCodigo());
		} else {
			return procedureRepository.distribuirCuota(Integer.parseInt(m.getId()), estimadomes, Integer.parseInt(ano),
					Integer.parseInt(mes), tipo, ter.get(0).getNit(), ref.get(0).getCodigo());
		}

		// return procedureRepository.distribuirCuota(linea, estimadomes, ano, mes,
		// tipo, ter.get(0).getCodigoLuc(), ref);
	}

	@GetMapping("/datoMes")
	public Integer[] datoMes(Integer mes) {
		String[] strMonths = new String[] { "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO",
				"SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" };
		Integer[] infoMes = new Integer[2];
		Calendar c = Calendar.getInstance();
		int anyo = c.get(Calendar.YEAR);
		// int mes = c.get(Calendar.MONTH) + 1;
		int numDias = 0;
		Meses datosMes = mesService.findByMes(strMonths[mes - 1]);

		switch (mes) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				numDias = 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				numDias = 30;
				break;
			case 2:
				if ((anyo % 4 == 0 && anyo % 100 != 0) || anyo % 400 == 0) {
					numDias = 29;
				} else {
					numDias = 28;
				}
				break;
			default:
				System.out.println("\nEl mes " + mes + " es incorrecto.");
				break;
		}

		infoMes[0] = numDias;

		numDias = numDias - datosMes.getDomingos() - datosMes.getFestivos();

		infoMes[1] = numDias;

		return infoMes;
	}

	/*
	 * @GetMapping("/preOrden/{marca}") public void preOrden(@PathVariable String
	 * marca) throws ParseException, IOException {
	 * 
	 * List<PreOrden> preOc = new ArrayList<PreOrden>(); Marca m =
	 * marcaService.findById(marca); Calendar calendar = Calendar.getInstance();
	 * Calendar calendarAct = Calendar.getInstance(); Calendar calendarFut =
	 * Calendar.getInstance(); Calendar calendarDiferencia = Calendar.getInstance();
	 * Date f = new Date();
	 * 
	 * Integer[] infoMesAct = datoMes(f.getMonth() + 1 ); Integer[] infoMesSgte =
	 * datoMes(f.getMonth() + 2 ); Date today = calendar.getTime();
	 * calendar.add(Calendar.DATE, -m.getLeadTime()); Date back =
	 * calendar.getTime();
	 * 
	 * //VALIDA SI YA UNA ORDEN CREADA List<Ped> p =
	 * pedService.findExistente(m.getNit(), back, today); List<DetalleOCPed> dp =
	 * null; if (p.size() > 0) { dp =
	 * detPedService.findByDetalle(p.get(0).getPedKey().getNumero(),
	 * p.get(0).getTipo()); }
	 * 
	 * //System.out.println("Fecha Pedido:" + calendarAct.getTime());
	 * calendarFut.add(Calendar.DATE, m.getLeadTime());
	 * 
	 * int domingos = 0; while (calendarDiferencia.before(calendarFut) ||
	 * calendarDiferencia.equals(calendarFut)) { if
	 * (calendarDiferencia.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	 * domingos++; } calendarDiferencia.add(Calendar.DATE, 1); }
	 * 
	 * calendarDiferencia.add(Calendar.DATE, domingos);
	 * //System.out.println("Fecha Recibo:" + calendarDiferencia.getTime());
	 * 
	 * if (calendarAct.get(Calendar.MONTH) !=
	 * calendarDiferencia.get(Calendar.MONTH)) {
	 * System.out.println("Meses diferentes"); } else {
	 * 
	 * calendarAct.set(Calendar.DAY_OF_MONTH,
	 * calendarDiferencia.get(Calendar.DAY_OF_MONTH));
	 * calendarFut.set(Calendar.DAY_OF_MONTH,
	 * calendarDiferencia.get(Calendar.DAY_OF_MONTH));
	 * 
	 * calendarFut.add(Calendar.DATE, m.getTimeRepo()); domingos = 0;
	 * 
	 * while (calendarDiferencia.before(calendarFut) ||
	 * calendarDiferencia.equals(calendarFut)) { if
	 * (calendarDiferencia.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	 * domingos++; } calendarDiferencia.add(Calendar.DATE, 1); }
	 * 
	 * calendarDiferencia.add(Calendar.DATE, domingos);
	 * System.out.println("Fecha de Finalizacion de la mercancia: " +
	 * calendarDiferencia.getTime());
	 * 
	 * List<Object[]> listaMesAct = estimadoService.findPrueba(marca, (f.getMonth()
	 * + 1));
	 * 
	 * for (Object[] detListaMesAct : listaMesAct) { double invSegurity = 0;
	 * 
	 * calendarAct.set(Calendar.DAY_OF_MONTH,
	 * calendarDiferencia.get(Calendar.DAY_OF_MONTH));
	 * calendarFut.set(Calendar.DAY_OF_MONTH,
	 * calendarDiferencia.get(Calendar.DAY_OF_MONTH));
	 * 
	 * calendarFut.add(Calendar.DATE, (int) detListaMesAct[6]); domingos = 0;
	 * 
	 * while (calendarDiferencia.before(calendarFut) ||
	 * calendarDiferencia.equals(calendarFut)) { if
	 * (calendarDiferencia.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	 * domingos++; } calendarDiferencia.add(Calendar.DATE, 1); }
	 * 
	 * if (calendarAct.get(Calendar.MONTH) !=
	 * calendarDiferencia.get(Calendar.MONTH)) {
	 * 
	 * List<Object[]> r2 = null; System.out.println("Fecha de dif nuevamente: " +
	 * calendarDiferencia.getTime()); System.out.println("Fecha de act nuevamente: "
	 * + calendarAct.getTime()); String codigo = (String) detListaMesAct[3];
	 * 
	 * int difMes = infoMesAct[0] - calendarAct.get(Calendar.DAY_OF_MONTH); int
	 * restanteMes = (int) detListaMesAct[6] - difMes;
	 * 
	 * int invSegMesAct = 0; int invSegMesSgte = 0;
	 * 
	 * invSegMesSgte = (int) detListaMesAct[6] - invSegMesAct;
	 * 
	 * r2 = estimadoService.findEstRef((f.getMonth() + 1) + 2, codigo); Integer[]
	 * infoMesSgte2 = datoMes((f.getMonth() + 1) + 2); invSegurity = (invSegMesSgte
	 * * (((double) (long) r2.get(0)[4]) / infoMesSgte2[1]));
	 * 
	 * }
	 * 
	 * 
	 * 
	 * }
	 * 
	 * }
	 * 
	 * //diasMesAct = calendarAct.get(Calendar.DAY_OF_MONTH);
	 * 
	 * }
	 */

	@GetMapping("/preOrden/{marca}")
	public void preOrden(@PathVariable String marca) throws ParseException, IOException {

		Date f = new Date();
		Integer[] infoMes = datoMes(f.getMonth() + 1);

		Marca m = marcaService.findById(marca);

		Calendar exist = Calendar.getInstance();
		Date today = exist.getTime();
		exist.add(Calendar.DATE, -m.getLeadTime());
		Date back = exist.getTime();

		List<Ped> p = pedService.findExistente(m.getNit(), back, today);
		List<DetalleOCPed> dp = null;
		if (p.size() > 0) {
			dp = detPedService.findByDetalle(p.get(0).getPedKey().getNumero(), p.get(0).getTipo());
		}

		List<PreOrden> preOc = new ArrayList<PreOrden>();
		List<Object[]> listaMesAct = null;
		List<Object[]> r2 = null;

		Calendar calendarAct = Calendar.getInstance();
		Calendar calendarFut = Calendar.getInstance();

		System.out.println("Fecha Pedido:" + calendarAct.getTime());

		int fechaRecibo = calendarAct.get(Calendar.DAY_OF_MONTH) + m.getLeadTime();

		calendarFut.set(Calendar.DATE, m.getLeadTime());
		// calendarFut.set(Calendar.DAY_OF_MONTH, fechaRecibo);

		int domingosRecibo = 0;
		int domingosPreOrden = 0;
		int mesSgte = 0;
		int mesAct = 0;
		int diasMesAct = 0;
		int diasMesSgte = 0;

		while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
			if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				domingosRecibo++;
			}
			calendarAct.add(Calendar.DATE, 1);
		}

		calendarAct.set(Calendar.DAY_OF_MONTH, (fechaRecibo + domingosRecibo));
		System.out.println("Fecha Recibo:" + calendarAct.getTime());

		diasMesAct = calendarAct.get(Calendar.DAY_OF_MONTH);

		int fechaReciboProxPed = calendarAct.get(Calendar.DAY_OF_MONTH) + m.getTimeRepo();
		Integer[] infoMesSgte = datoMes((f.getMonth()) + 1);
		Integer[] infoMesSgte2 = datoMes((f.getMonth() + 1) + 2);

		if (fechaReciboProxPed > infoMes[0]) {

			mesAct = infoMes[0] - fechaRecibo - domingosRecibo;
			mesSgte = fechaReciboProxPed - infoMes[0];

			diasMesAct = mesAct;
			diasMesSgte = mesSgte;

			calendarFut.set(Calendar.DAY_OF_MONTH, infoMes[0]);

			while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
				if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					domingosPreOrden++;
				}
				calendarAct.add(Calendar.DATE, 1);
			}

			diasMesAct = diasMesAct - domingosPreOrden;
			diasMesSgte = m.getTimeRepo() - diasMesAct;

			calendarFut.set(Calendar.MONTH, calendarFut.get(Calendar.MONTH) + 1);
			calendarFut.set(Calendar.DAY_OF_MONTH, mesSgte);

			while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
				if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					domingosPreOrden++;
				}
				calendarAct.add(Calendar.DATE, 1);
			}

			calendarAct.set(Calendar.DAY_OF_MONTH, mesSgte);
			calendarFut.set(Calendar.DAY_OF_MONTH, (mesSgte + domingosPreOrden));

			mesSgte = mesSgte + domingosPreOrden;
			domingosPreOrden = 0;

			while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
				if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					domingosPreOrden++;
				}
				calendarAct.add(Calendar.DATE, 1);
			}

			calendarFut.set(Calendar.DAY_OF_MONTH, (mesSgte + domingosPreOrden));
			int mes1 = calendarFut.get(Calendar.MONTH);
			int dia1 = calendarFut.get(Calendar.DAY_OF_MONTH);
			System.out.println("Fecha de Finalizacion de la mercancia: " + calendarFut.getTime());

			int suma = mesSgte + domingosPreOrden;
			domingosPreOrden = 0;

			calendarAct.set(Calendar.DAY_OF_MONTH, (suma - m.getLeadTime()));

			while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
				if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					domingosPreOrden++;
				}
				calendarAct.add(Calendar.DATE, 1);
			}

			calendarAct.set(Calendar.DAY_OF_MONTH, (suma - m.getLeadTime()) - domingosPreOrden);

			System.out.println("Fecha de Preorden: " + calendarAct.getTime());

			listaMesAct = estimadoService.findPrueba(marca, (f.getMonth() + 1));

			for (Object[] detListaMesAct : listaMesAct) {

				String codigo = (String) detListaMesAct[3];
				double total = 0;
				double cntMesAct = 0;
				double cntMesSgte = 0;
				double invSegurity = 0;
				double stock = 0;

				calendarFut.add(Calendar.DATE, (int) detListaMesAct[6]);
				calendarAct.set(Calendar.DAY_OF_MONTH, dia1);

				domingosPreOrden = 0;
				while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
					if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						domingosPreOrden++;
					}
					calendarAct.add(Calendar.DATE, 1);
				}

				calendarFut.add(Calendar.DATE, domingosPreOrden);
				calendarAct.set(Calendar.MONTH, mes1);
				calendarAct.set(Calendar.DAY_OF_MONTH, dia1);
				int invSegMesAct = 0;
				int invSegMesSgte = 0;

				if (calendarAct.get(Calendar.MONTH) != calendarFut.get(Calendar.MONTH)) {

					while (calendarAct.get(Calendar.DAY_OF_MONTH) != infoMesSgte[0]) {
						if (calendarAct.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
							invSegMesAct++;
						}
						calendarAct.add(Calendar.DATE, 1);
					}

					invSegMesSgte = (int) detListaMesAct[6] - invSegMesAct;

					r2 = estimadoService.findEstRef((f.getMonth() + 1) + 2, codigo);
					if (r2.size() > 0) {
						invSegurity = (invSegMesSgte * (((double) (long) r2.get(0)[4]) / infoMesSgte2[1]));
					}

				} else {
					invSegMesAct = (int) detListaMesAct[6];
				}

				System.out.println(calendarAct.getTime());
				System.out.println(calendarFut.getTime());
				System.out.println("--------------------");
				System.out.println(calendarAct.get(Calendar.MONTH));
				System.out.println(calendarFut.get(Calendar.MONTH));

				List<Object[]> r = estimadoService.findEstRef((f.getMonth() + 1) + 1, codigo);
				Object findStock = estimadoService.findStockActual(codigo);

				System.out.println(r);

				if (findStock == null) {
					stock = 0;
				} else {
					stock = new Double(findStock.toString());
				}

				if (r.size() > 0) {
					cntMesSgte = diasMesSgte * (((double) (long) r.get(0)[4]) / infoMesSgte[1]);
					invSegurity = invSegurity + (invSegMesAct * (((double) (long) r.get(0)[4]) / infoMesSgte[1]));
				} else {
					cntMesSgte = 0;
					invSegurity = invSegurity + (invSegMesAct * (((double) (long) detListaMesAct[7]) / infoMesSgte[1]));
				}

				cntMesAct = diasMesAct * (((double) (long) detListaMesAct[7]) / infoMes[1]);
				total = cntMesAct + cntMesSgte + invSegurity - stock;

				if (dp != null) {
					for (int i = 0; i < dp.size(); i++) {
						if (dp.get(i).getRefe().getCodigo().equals((String) detListaMesAct[3])) {
							total = total - dp.get(i).getCantidad();
						}
					}
				}

				if (total > 0) {

					PreOrden pre = new PreOrden();
					pre.setTipo((String) detListaMesAct[0]);
					pre.setAnio((Integer) detListaMesAct[1]);
					pre.setMes((Integer) detListaMesAct[2]);
					pre.setCodigo((String) detListaMesAct[3]);
					pre.setDescripcion((String) detListaMesAct[4]);
					pre.setMarca(m);
					pre.setTimeRepo(m.getTimeRepo());
					pre.setLeadTime(m.getLeadTime());
					pre.setTipoRef((String) detListaMesAct[5]);

					pre.setCntEstimado((int) Math.round(total));
					pre.setStock(stock);
					preOc.add(pre);
				}

				calendarFut.set(Calendar.MONTH, mes1);
				calendarFut.set(Calendar.DAY_OF_MONTH, dia1);
				calendarAct.set(Calendar.MONTH, mes1);
				calendarAct.set(Calendar.DAY_OF_MONTH, dia1);

			}

		} else {

			calendarFut.set(Calendar.MONTH, calendarAct.get(Calendar.MONTH));
			calendarFut.set(Calendar.DAY_OF_MONTH, calendarAct.get(Calendar.DAY_OF_MONTH) + m.getTimeRepo());

			diasMesAct = diasMesAct - domingosPreOrden;

			while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
				if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					domingosPreOrden++;
				}
				calendarAct.add(Calendar.DATE, 1);
			}

			domingosPreOrden = 0;

			while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
				if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					domingosPreOrden++;
				}
				calendarAct.add(Calendar.DATE, 1);
			}

			int mes1 = calendarFut.get(Calendar.MONTH);
			int dia1 = calendarFut.get(Calendar.DAY_OF_MONTH);
			System.out.println("Fecha de Finalizacion de la mercancia: " + calendarFut.getTime());
			diasMesAct = calendarFut.get(Calendar.DAY_OF_MONTH) - diasMesAct;

			calendarAct.set(Calendar.DAY_OF_MONTH, (calendarFut.get(Calendar.DAY_OF_MONTH) - m.getLeadTime()));

			int d = 0;
			if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				d++;
			}

			calendarAct.set(Calendar.DAY_OF_MONTH, (calendarFut.get(Calendar.DAY_OF_MONTH) - m.getLeadTime()) - d);

			System.out.println("Fecha de Preorden: " + calendarAct.getTime());

			listaMesAct = estimadoService.findPrueba(marca, (f.getMonth() + 1));

			for (Object[] detListaMesAct : listaMesAct) {

				String codigo = (String) detListaMesAct[3];
				double total = 0;
				double cntMesAct = 0;
				double cntMesSgte = 0;
				double invSegurity = 0;
				double stock = 0;

				calendarFut.add(Calendar.DATE, (int) detListaMesAct[6]);
				calendarAct.set(Calendar.DAY_OF_MONTH, dia1);

				domingosPreOrden = 0;
				while (calendarAct.before(calendarFut) || calendarAct.equals(calendarFut)) {
					if (calendarAct.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						domingosPreOrden++;
					}
					calendarAct.add(Calendar.DATE, 1);
				}

				calendarFut.add(Calendar.DATE, domingosPreOrden);
				calendarAct.set(Calendar.MONTH, mes1);
				calendarAct.set(Calendar.DAY_OF_MONTH, dia1);
				int invSegMesAct = 0;
				int invSegMesSgte = 0;

				if (calendarAct.get(Calendar.MONTH) != calendarFut.get(Calendar.MONTH)) {

					while (calendarAct.get(Calendar.DAY_OF_MONTH) != infoMesSgte[0]) {
						if (calendarAct.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
							invSegMesAct++;
						}
						calendarAct.add(Calendar.DATE, 1);
					}

					invSegMesSgte = (int) detListaMesAct[6] - invSegMesAct;

					r2 = estimadoService.findEstRef((f.getMonth() + 1) + 2, codigo);

					if (r2.size() > 0) {
						invSegurity = (invSegMesSgte * (((double) (long) r2.get(0)[4]) / infoMesSgte2[1]));
					}

				} else {
					invSegMesAct = (int) detListaMesAct[6];
				}

				System.out.println(calendarAct.getTime());
				System.out.println(calendarFut.getTime());
				System.out.println("--------------------");
				System.out.println(calendarAct.get(Calendar.MONTH));
				System.out.println(calendarFut.get(Calendar.MONTH));

				List<Object[]> r = estimadoService.findEstRef((f.getMonth() + 1) + 1, codigo);
				Object findStock = estimadoService.findStockActual(codigo);

				System.out.println(r);

				if (findStock == null) {
					stock = 0;
				} else {
					stock = new Double(findStock.toString());
				}

				if (r.size() > 0) {
					cntMesSgte = diasMesSgte * (((double) (long) r.get(0)[4]) / infoMesSgte[1]);
					invSegurity = invSegurity + (invSegMesAct * (((double) (long) r.get(0)[4]) / infoMesSgte[1]));
				} else {
					cntMesSgte = 0;
					invSegurity = invSegurity + (invSegMesAct * (((double) (long) detListaMesAct[7]) / infoMesSgte[1]));
				}

				cntMesAct = diasMesAct * (((double) (long) detListaMesAct[7]) / infoMes[1]);
				total = cntMesAct + cntMesSgte + invSegurity - stock;

				if (dp != null) {
					for (int i = 0; i < dp.size(); i++) {
						if (dp.get(i).getRefe().getCodigo().equals((String) detListaMesAct[3])) {
							total = total - dp.get(i).getCantidad();
						}
					}
				}

				if (total > 0) {

					PreOrden pre = new PreOrden();
					pre.setTipo((String) detListaMesAct[0]);
					pre.setAnio((Integer) detListaMesAct[1]);
					pre.setMes((Integer) detListaMesAct[2]);
					pre.setCodigo((String) detListaMesAct[3]);
					pre.setDescripcion((String) detListaMesAct[4]);
					pre.setMarca(m);
					pre.setTimeRepo(m.getTimeRepo());
					pre.setLeadTime(m.getLeadTime());
					pre.setTipoRef((String) detListaMesAct[5]);
					pre.setCntEstimado((int) Math.round(total));
					pre.setStock(stock);
					preOc.add(pre);
				}

				calendarFut.set(Calendar.MONTH, mes1);
				calendarFut.set(Calendar.DAY_OF_MONTH, dia1);
				calendarAct.set(Calendar.MONTH, mes1);
				calendarAct.set(Calendar.DAY_OF_MONTH, dia1);

			}

		}

		obtenerCompra(preOc);

	}

	public void obtenerCompra(List<PreOrden> preOrc) throws ParseException {

		Date f = new Date();

		try {

			String anioNum = "";
			String mesNum = "";
			String nitProv = "";
			String prefijo = "";
			String operacion = "";
			Double nitSin = 0.0;
			Date myDate = new Date();
			SimpleDateFormat mdyFormat = new SimpleDateFormat("MM/dd/yyyy");
			String fecha = mdyFormat.format(myDate);
			Date date = mdyFormat.parse(fecha);
			Calendar c = Calendar.getInstance();
			Ped pOC;
			Referencias refIns;
			Um u;
			int secu = 1;

			for (int i = 0; i < preOrc.size(); i++) {

				if (i == 0) {
					anioNum = String.valueOf(preOrc.get(i).getAnio()).substring(2, 4);
					mesNum = String.valueOf(preOrc.get(i).getMes());
					if (Integer.parseInt(mesNum) < 10) {
						mesNum = "0" + mesNum;
					}

					nitProv = preOrc.get(i).getMarca().getNit();
					nitSin = Double.parseDouble(nitProv);
					prefijo = preOrc.get(i).getTipo();
					if (prefijo.equals("LUC")) {
						prefijo = "ORC";
					} else {
						prefijo = "OPP";
					}

					operacion = anioNum + mesNum + String.valueOf(c.get(Calendar.DAY_OF_MONTH))
							+ String.valueOf(c.get(Calendar.HOUR)) + String.valueOf(c.get(Calendar.MINUTE));

					pOC = pedService.findbyPed(Integer.parseInt(operacion), prefijo);

					if (pOC == null) {

						Ped ped = new Ped();
						ped.setAnulado(0);
						ped.setTercero(tercerosService.findByTer(nitProv));
						ped.setSo_id(operacion);
						ped.setFecha(date);
						ped.setFechaHora(date);
						ped.setFechaHoraEnt(date);
						ped.setNitDestino(nitSin);
						ped.setPc("API");
						ped.setUsuario("API");
						ped.setVendedor(tercerosService.findByTer(nitProv));
						ped.setCondicion("21");
						ped.setCodigoDir(1);
						ped.setCodigoDirDest(1);
						ped.setProveedor(nitProv);
						ped.setTipo(prefijo);
						ped.setPedKey(new PedKey(3, 15, Integer.parseInt(operacion)));
						ped.setEstado("RE");
						pedService.savePed(ped);

						PedHis pedHis = new PedHis();
						pedHis.setAnulado(0);
						pedHis.setTercero(tercerosService.findByTer(nitProv));
						pedHis.setFecha(date);
						pedHis.setFechaHora(date);
						pedHis.setFechaHoraEnt(date);
						pedHis.setNitDestino(nitSin);
						pedHis.setPc("API");
						pedHis.setUsuario("API");
						pedHis.setVendedor(tercerosService.findByTer(nitProv));
						pedHis.setCondicion("21");
						pedHis.setCodigoDir(1);
						pedHis.setProveedor(nitProv);
						pedHis.setTipo(prefijo);
						pedHis.setPedKey(new PedKey(3, 15, Integer.parseInt(operacion)));
						pedHisService.save(pedHis);
					}
				}

				refIns = refeService.findByRefe(preOrc.get(i).getCodigo());
				String codigo = refIns.getCodigo();

				DetalleOCPed docPed = new DetalleOCPed();
				docPed.setSw(3);
				docPed.setBodega(15);
				docPed.setNumero(Integer.parseInt(operacion));
				docPed.setRefe(refIns);
				docPed.setSeq(secu);

				u = umService.findByUndMin(codigo, "CJ");
				if (u != null) {
					docPed.setCantidadUnd((double) u.getConv());
				} else {
					docPed.setCantidadUnd(1.0);
				}

				double cntCj = Math.ceil((preOrc.get(i).getCntEstimado()) / (docPed.getCantidadUnd()));
				if (cntCj == 0) {
					cntCj = 1;
				}

				docPed.setCantidad((int) (cntCj * docPed.getCantidadUnd()));
				docPed.setCantidadDes((cntCj * docPed.getCantidadUnd()));
				docPed.setValorUni(refIns.getValorUni());
				docPed.setPorcentajeIva((double) refIns.getIva());
				docPed.setPorcentajeDes(0.0);
				docPed.setUnd("CJ");
				docPed.setTipo(prefijo);
				docPed.setEstado("x");
				docPed.setStock((preOrc.get(i).getStock()).intValue());

				Object prom = detOcServices.findByPromVenta("FAC", codigo, (f.getYear() + 1900), f.getMonth());
				Object count = detOcServices.findByCount("FAC", codigo, (f.getYear() + 1900), f.getMonth());
				double promedio = 0;

				if (prom != null) {
					promedio = ((double) prom / (long) count);
					docPed.setInvSeg((int) Math.round(Math.round(docPed.getCantidad()) / promedio));
				} else {
					docPed.setInvSeg((int) promedio);
				}

				// docPed.setInvSeg(preOrc.get(i).getDiasInv());
				docPedService.saveDetOCPed(docPed);
				secu++;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* FIND ALL */

	@GetMapping("/cargarVidas")
	public List<Terceros> cargarVidas() {
		return (List<Terceros>) tercerosService.findByVidas();
	}

	@GetMapping("/cargarRefTipo")
	public List<ReferenciasTipo> cargarRefTipo() {
		return (List<ReferenciasTipo>) refeTipoService.findAll();
	}

	@GetMapping("/cargarPreOrden")
	public List<Ped> cargarPreOrden() {
		return (List<Ped>) pedService.findEnProceso();
	}

	@GetMapping("/cargarOrdenes")
	public List<Ped> cargarOrdenes() {
		return (List<Ped>) pedService.findEnTransito();
	}

	@GetMapping("/detPreOrden/{numero}/{tipo}")
	public List<DetalleOCPed> detPreOrden(@PathVariable Integer numero, @PathVariable String tipo) {
		return (List<DetalleOCPed>) detPedService.findByDetalle(numero, tipo);
	}

	@GetMapping("/cargaAllRefe")
	public List<Referencias> cargaRefe() {
		return (List<Referencias>) refeService.findAll();
	}

	@GetMapping("/cargaAllCli")
	public List<Terceros> cargaCliTotal() {
		return (List<Terceros>) tercerosService.findAll();
	}

	@GetMapping("/cargaMar")
	public List<Marca> cargaMar() {
		return (List<Marca>) marcaService.findAll();
	}

	@GetMapping("/cargarAyudante")
	public List<Terceros> cargarAyudante() {
		return (List<Terceros>) tercerosService.findByAyudante();
	}

	@GetMapping("/cargarConductor")
	public List<Terceros> cargarConductor() {
		return (List<Terceros>) tercerosService.findByConductor();
	}

	// CARGA DATOS DE LA PLANILLA
	@GetMapping("/cargarDatos/{numPlanilla}")
	public Object[] cargarDatos(@PathVariable String numPlanilla) {
		return (Object[]) planillaLinService.findDatos(numPlanilla);
	}

	// CARGA VEHICULOS
	@GetMapping("/cargarVehiculos")
	public List<Vehiculo> cargarVehiculos() {
		return (List<Vehiculo>) vehiculoService.findAll();
	}

	// MOSTRAR DATOS PLANILLA
	@GetMapping("/cargarPlanilla")
	public List<Planilla> cargarPlanilla() {
		return (List<Planilla>) planillaService.findAll();
	}

	// MOSTRAR DATOS VEHICULO
	@GetMapping("/cargarVehiculo")
	public List<Vehiculo> cargarVehiculo() {
		return (List<Vehiculo>) vehiculoService.findAll();
	}

	// MOSTRAR DATOS CLASE DE VEHICULO
	@GetMapping("/cargarClaseVehiculo")
	public List<ClaseVehiculo> cargarClaseVehiculo() {
		return (List<ClaseVehiculo>) clasevehiculoService.findAll();
	}

	// MOSTRAR DATOS TIPO DE VEHICULO
	@GetMapping("/cargarTipoVehiculo")
	public List<TipoVehiculo> cargarTipoVehiculo() {
		return (List<TipoVehiculo>) tipovehiculoService.findAll();
	}

	/* FIND BY TEXTO */

/* 	@GetMapping("/cargarPresupuestoProveedor/{vendedor}/{mes}")
	public List<Object[]> cargarPresupuestoProveedor(@PathVariable String vendedor, @PathVariable String mes) {
		String datosFecha[] = mes.split("-");
		return (List<Object[]>) estimadoService.findPresupuestoProveedor(Double.parseDouble(vendedor),
				Integer.parseInt(datosFecha[1]));
	}

	@GetMapping("/cargarCntFacDevProv/{vendedor}/{marca}/{mes}")
	public List<Object[]> cargarCntFacDevProv(@PathVariable String vendedor, @PathVariable String marca,
			@PathVariable String mes) throws ParseException {

		String datosFecha[] = mes.split("-");
		List<Object[]> p = tercerosService.findCntFacDevProv(Double.parseDouble(vendedor), marca,
				Integer.parseInt(datosFecha[0]), Integer.parseInt(datosFecha[1]));

		return p;
	}

	@GetMapping("/cargarCntFacDevCliProv/{vendedor}/{nit}/{marca}/{mes}")
	public List<Object[]> cargarCntFacDevCliProv(@PathVariable String vendedor, @PathVariable String marca,
			@PathVariable String nit, @PathVariable String mes) throws ParseException {

		String datosFecha[] = mes.split("-");
		List<Object[]> p = tercerosService.findCntFacDevCliProv(Double.parseDouble(vendedor), marca,
				Double.parseDouble(nit), Integer.parseInt(datosFecha[0]), Integer.parseInt(datosFecha[1]));

		return p;
	}

	@GetMapping("/cargarPresupuestoCliente/{vendedor}/{mes}")
	public List<Object[]> cargarPresupuestoCliente(@PathVariable String vendedor, @PathVariable String mes) {
		String datosFecha[] = mes.split("-");
		return (List<Object[]>) estimadoService.findPresupuestoCliente(Double.parseDouble(vendedor),
				Integer.parseInt(datosFecha[1]));
	}

	@GetMapping("/cargarDetalleCliente/{vendedor}/{mes}/{nit}")
	public List<Object[]> cargarDetalleCliente(@PathVariable String vendedor, @PathVariable String mes,
			@PathVariable Double nit) {
		String datosFecha[] = mes.split("-");
		return (List<Object[]>) estimadoService.findDetalleCliente(Double.parseDouble(vendedor),
				Integer.parseInt(datosFecha[1]), nit);
	} */

	// CONSULTAR UNIDAD DE MEDIDA POR CODIGO
	@GetMapping("/consultarUm/{codigo}")
	public List<Um> consultarUm(@PathVariable String codigo) {
		return (List<Um>) umService.findById(codigo);
	}

	// CARGAR REFERENCIA POR DESCRIPCION
	@GetMapping("/cargaRefe/{descrip}")
	public List<Referencias> cargaRefe(@PathVariable String descrip) {
		return (List<Referencias>) refeService.findByTexto(descrip);
	}

	// CARGAR CLIENTE POR DESCRIPCION
	@GetMapping("/cargaCli/{descrip}")
	public List<Terceros> cargaCli(@PathVariable String descrip) {
		return (List<Terceros>) tercerosService.findByTexto(descrip);
	}


	// CARGAR LA RUTA DE UN VENDEDOR
	@GetMapping("/cargarRuta/{vendedor}")
	public List<Terceros> cargarRuta(@PathVariable Double vendedor) {
		return (List<Terceros>) tercerosService.findByRuta(vendedor);
	}

	// CARGA MARCA DE ACUERDO A LA DESCRIPCION
	@GetMapping("/cargaMar/{descrip}")
	public List<Marca> cargaMarFill(@PathVariable String descrip) {
		return (List<Marca>) marcaService.findByTexto(descrip);
	}

	// CARGA LOS CLIENTES QUE LLEGARAN DE VITA AVE EL DIA SELECCIONADO
	@GetMapping("/cargaCliDia/{dia}")
	public List<Terceros> cargaCliDia(@PathVariable Integer dia) {
		Calendar now = Calendar.getInstance();
		int dayOfMonth = now.get(Calendar.DAY_OF_MONTH);
		String[] strDays = new String[] { "DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO" };

		if (dia == dayOfMonth) {
			return (List<Terceros>) tercerosService.findByDia(strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
		} else {
			dia = dia - dayOfMonth;
			now.add(Calendar.DATE, dia);
			return (List<Terceros>) tercerosService.findByDia(strDays[now.getTime().getDay()]);
		}

	}

	// CARGAR CLIENTES AGENDADOS
	@GetMapping("/cargaCliDiaAgen/{proveedor}")
	public List<Terceros> cargaCliDiaAgen(@PathVariable String proveedor) {

		List<Ped> Ped = pedService.findCliAgen(proveedor);
		List<Terceros> ter = new ArrayList<>();
		for (int i = 0; i < Ped.size(); i++) {
			ter.add(Ped.get(i).getTercero());
			ter.get(i).setAutoretenedor(Ped.get(i).getPedKey().getNumero());

		}
		return ter;
	}

	// MODIFICAR ESTADO DE RECEPCION A FINALIZADO
	@GetMapping("/modificarEstadoRec/{pedido}")
	private void modificarEstadoRec(@PathVariable String pedido) {
		Ped pedRec = pedService.findbyId(pedido);
		pedRec.setEstado("FI");
		pedService.savePed(pedRec);
	}

	// RECHAZAR ORDEN DE COMPRA
	@GetMapping("/rechazarOrc/{numero}/{tipo}")
	public void rechazarOrc(@PathVariable Integer numero, @PathVariable String tipo) {
		Ped orc = pedService.findbyPed(numero, tipo);
		orc.setEstado("FI");
		pedService.savePed(orc);
	}

	// MOSTRAR LA AGENDA DE CADA PLANILLA
	@GetMapping("/cargaPlanillaDias/{day}/{month}/{year}")
	public List<Planilla> cargaPlanillaDias(@PathVariable String day, @PathVariable String month,
			@PathVariable String year) {
		if (Integer.parseInt(month) < 10 && Integer.parseInt(day) < 10) {
			month = "0" + month;
			day = "0" + day;
		}

		String f = month + day + year;
		return (List<Planilla>) planillaService.findByPD(f);
	}

	// CARGAR DETALLE DE LA PLANILLA
	@GetMapping("/cargarDetPlanilla/{numPlanilla}/{nit}")
	public List<PlanillaLin> cargarDetPlanilla(@PathVariable String numPlanilla, @PathVariable String nit) {
		return (List<PlanillaLin>) planillaLinService.findByNumPl(numPlanilla, Double.parseDouble(nit));
	}

	// MOSTRAR EL DETALLE DE CADA ALMACEN, SIN REPETIRLO
	@GetMapping("/cargarAlmacen/{numPlanilla}")
	public List<Terceros> cargarAlmacen(@PathVariable String numPlanilla) {

		List<PlanillaLin> pl = planillaLinService.findByNumPlanAlm(numPlanilla);
		List<Terceros> al = new ArrayList<Terceros>();
		for (int i = 0; i < pl.size(); i++) {

			if (i == 0) {
				al.add(pl.get(i).getAlmacen());
			} else {
				if (!pl.get(i).getAlmacen().equals(pl.get(i - 1).getAlmacen())) {
					al.add(pl.get(i).getAlmacen());
				}
			}

		}

		return al;
	}

	@GetMapping("/infoTercero/{nit}")
	private String[] infoTercero(@PathVariable String nit) {

		String info[] = new String[2];
		Terceros ter = tercerosService.findByTer(nit);
		info[0] = ter.getListaPrecio();
		info[1] = String.valueOf(BigDecimal.valueOf(ter.getVendedor()));

		return info;
	}

	// PERMITE LOGEO DE USUARIOS
	@GetMapping("/login/{username}/{password}")
	@ResponseStatus(HttpStatus.OK)
	public User Login(@PathVariable String username, @PathVariable String password) {
		  jdbcTemplate.query("exec [WMS2_SP_LOGGIN_ANGULAR] ?, ?", new Object[] { username, password },
			new BeanPropertyRowMapper<>(User.class));
			User usuarios = userService.findUserByUserName(username);
			return usuarios;
	}


	@GetMapping("/cargaRefeMarca/{proveedor}")
	public List<Referencias> cargaRefeMarca(@PathVariable String proveedor) {
		return (List<Referencias>) refeService.findByRefeProv(proveedor);
	}

	// CARGAR CLIENTE POR VENDEDOR
	@GetMapping("/cargaCliVendedor/{vendedor}")
	public List<Terceros> cargaCliVendedor(@PathVariable String vendedor) {
		Terceros nombreUsuario = tercerosService.findByTerTexto(vendedor);
		return (List<Terceros>) tercerosService.findByRutaVendedor(nombreUsuario.getNit());
	}
}
