package com.luc.logistica.crearEntrada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Tbl_WMS2_Productos_Ubicados")
public class ProdUbicado {

    @Id
    @Column(name = "Id")
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "Id_Ubicacion")
    private Ubicaciones idUbi;
    /*
     * @ManyToOne
     * 
     * @JoinColumn(name = "Id_Referencia") private Referencias idRefe;
     */
    @Column(name = "Id_Referencia")
    private Integer idRefe;
    @ManyToOne
    @JoinColumn(name = "Id_Lote")
    private Lote idLote;
    @Column(name = "Cantidad_Entrada")
    private Double cntEntrada;
    @Column(name = "Cantidad_Salida")
    private Double cntSalida;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Ubicaciones getIdUbi() {
        return idUbi;
    }

    public void setIdUbi(Ubicaciones idUbi) {
        this.idUbi = idUbi;
    }

    public Lote getIdLote() {
        return idLote;
    }

    public void setIdLote(Lote idLote) {
        this.idLote = idLote;
    }

    public Double getCntEntrada() {
        return cntEntrada;
    }

    public void setCntEntrada(Double cntEntrada) {
        this.cntEntrada = cntEntrada;
    }

    public Double getCntSalida() {
        return cntSalida;
    }

    public void setCntSalida(Double cntSalida) {
        this.cntSalida = cntSalida;
    }

    public Integer getIdRefe() {
        return idRefe;
    }

    public void setIdRefe(Integer idRefe) {
        this.idRefe = idRefe;
    }

}
