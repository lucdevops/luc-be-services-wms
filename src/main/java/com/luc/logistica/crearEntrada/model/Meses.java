package com.luc.logistica.crearEntrada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "meses_mstr")
public class Meses {
    @Id
    @Column(name = "mes")
    private String mes;
    @Column(name = "domingos")
    private Integer domingos;
    @Column(name = "festivos")
    private Integer festivos;

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Integer getDomingos() {
        return domingos;
    }

    public void setDomingos(Integer domingos) {
        this.domingos = domingos;
    }

    public Integer getFestivos() {
        return festivos;
    }

    public void setFestivos(Integer festivos) {
        this.festivos = festivos;
    }

}
