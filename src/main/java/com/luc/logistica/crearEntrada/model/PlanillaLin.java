package com.luc.logistica.crearEntrada.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "planilla_lin")
public class PlanillaLin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;
    @ManyToOne
    @JoinColumn(name = "almacen")
    private Terceros almacen;
    @Column(name = "proveedor")
    private String proveedor;
    @Column(name = "orden_de_compra")
    private String oc;
    @Column(name = "factura")
    private String factura;
    @Column(name = "observaciones")
    private String observaciones;
    @Column(name = "fecha")
    private Date fecha;
    @Column(name = "num_planilla")
    private String numPlanilla;
    @Column(name = "num_pedido")
    private String numPedido;
    @Column(name = "num_guia")
    private String numGuia;
    @Column(name = "num_cita")
    private String numCita;
    @Column(name = "hora_cita")
    private String horaCita;
    @Column(name = "motivo")
    private String motivo;
    @Column(name = "empresa_transportadora")
    private String empresaTransp;
    @Column(name = "motivo_proveedor")
    private String motivoProv;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOc() {
        return oc;
    }

    public void setOc(String oc) {
        this.oc = oc;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNumPlanilla() {
        return numPlanilla;
    }

    public void setNumPlanilla(String numPlanilla) {
        this.numPlanilla = numPlanilla;
    }

    public Terceros getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Terceros almacen) {
        this.almacen = almacen;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(String numPedido) {
        this.numPedido = numPedido;
    }

    public String getNumGuia() {
        return numGuia;
    }

    public void setNumGuia(String numGuia) {
        this.numGuia = numGuia;
    }

    public String getNumCita() {
        return numCita;
    }

    public void setNumCita(String numCita) {
        this.numCita = numCita;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getEmpresaTransp() {
        return empresaTransp;
    }

    public void setEmpresaTransp(String empresaTransp) {
        this.empresaTransp = empresaTransp;
    }

    public String getMotivoProv() {
        return motivoProv;
    }

    public void setMotivoProv(String motivoProv) {
        this.motivoProv = motivoProv;
    }

}