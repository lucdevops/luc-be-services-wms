package com.luc.logistica.crearEntrada.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehiculo_mstr")
public class Vehiculo {
    @Id
    @Column(name = "placa")
    private String placa;
    @Column(name = "fecha_soat")
    private Date fecha_soat;
    @Column(name = "fecha_tecnicomecanica")
    private Date fecha_tecnicomecanica;
    @Column(name = "peso_carga")
    private Double peso_carga;
    @Column(name = "cubicaje")
    private Double cubicaje;
    @ManyToOne
    @JoinColumn(name = "id_tipo", columnDefinition = "Integer")
	private TipoVehiculo tipo;
    @ManyToOne
    @JoinColumn(name = "id_clase", columnDefinition = "Integer")
	private ClaseVehiculo clase;

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Date getFecha_soat() {
        return fecha_soat;
    }

    public void setFecha_soat(Date fecha_soat) {
        this.fecha_soat = fecha_soat;
    }

    public Date getFecha_tecnicomecanica() {
        return fecha_tecnicomecanica;
    }

    public void setFecha_tecnicomecanica(Date fecha_tecnicomecanica) {
        this.fecha_tecnicomecanica = fecha_tecnicomecanica;
    }

    public Double getPeso_carga() {
        return peso_carga;
    }

    public void setPeso_carga(Double peso_carga) {
        this.peso_carga = peso_carga;
    }

    public Double getCubicaje() {
        return cubicaje;
    }

    public void setCubicaje(Double cubicaje) {
        this.cubicaje = cubicaje;
    }

    public TipoVehiculo getTipo() {
        return tipo;
    }

    public void setTipo(TipoVehiculo tipo) {
        this.tipo = tipo;
    }

    public ClaseVehiculo getClase() {
        return clase;
    }

    public void setClase(ClaseVehiculo clase) {
        this.clase = clase;
    }

}
