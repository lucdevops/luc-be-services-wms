package com.luc.logistica.crearEntrada.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TercerosKey implements Serializable{
	
	@Column(name = "nit")
    protected Double nit;
    @Column(name = "codigo_luc")
    protected String codigoLuc;

    public TercerosKey (Double nit,String codigoLuc) {
    	super();
		this.nit = nit;
		this.codigoLuc = codigoLuc;
    }
    
    public TercerosKey() {}

	public Double getNit() {
		return nit;
	}
	public void setNit(Double nit) {
		this.nit = nit;
	}
	public String getCodigoLuc() {
		return codigoLuc;
	}
	public void setCodigoLuc(String codigoLuc) {
		this.codigoLuc = codigoLuc;
	}

}
